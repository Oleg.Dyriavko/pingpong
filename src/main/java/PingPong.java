public class PingPong implements Runnable {
    private String word;
    private final Object object;
    private PingPong(String word, Object object) {
        this.word = word;
        this.object = object;
    }

    public void run() {

        synchronized (object) {
            for (int i = 0; i < 10; i++) {
                System.out.println(word + i);
                try {

                    object.notifyAll();
                    object.wait();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
            }
        }
    }

    public static void main(String[] args) throws InterruptedException {

        Object object = new Object();
        Runnable p1 = new PingPong("ping - ", object);
        Thread t1 = new Thread(p1);
        t1.start();

        Runnable p2 = new PingPong("    PONG - ", object);
        Thread t2 = new Thread(p2);
        t2.start();

        Thread.sleep(2000);
        t1.interrupt();
        t2.interrupt();

    }
}
